package com.scainburger.letsmodreboot.reference;

public class Reference {
    public static final String MOD_ID = "LetsModReboot";
    public static final String MOD_NAME = "Let's Mod Reboot";
    public static final String MOD_VERSION = "1.0";
    public static final String MC_VERSION = "1.7.10";
    public static final String FULL_VERSION = MC_VERSION + "-" + MOD_VERSION;
}
