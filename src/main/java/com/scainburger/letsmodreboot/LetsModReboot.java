package com.scainburger.letsmodreboot;

import com.scainburger.letsmodreboot.configuration.ConfigurationHandler;
import com.scainburger.letsmodreboot.proxy.IProxy;
import com.scainburger.letsmodreboot.reference.Reference;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid=Reference.MOD_ID, name=Reference.MOD_NAME, version=Reference.FULL_VERSION)

public class LetsModReboot
{

    @Mod.Instance("LetsModReboot")
    public static LetsModReboot instance;

    @SidedProxy
    (
        clientSide = "com.scainburger.letsmodreboot.proxy.ClientProxy",
        serverSide = "com.scainburger.letsmodreboot.proxy.ServerProxy"
    )
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        ConfigurationHandler.init(event.getSuggestedConfigurationFile());
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {

    }

}
